﻿DROP SCHEMA IF EXISTS MouldVision CASCADE;
CREATE SCHEMA MouldVision;

CREATE TABLE MouldVision.users  (
id_user SERIAL PRIMARY KEY NOT NULL,
username CHAR(100) NOT NULL,
surname CHAR(100) NOT NULL
);

CREATE TABLE MouldVision.machines  (
id_machine SERIAL PRIMARY KEY NOT NULL,
name CHAR(100) NOT NULL
);

CREATE TABLE MouldVision.forms (
number INT PRIMARY KEY
);
CREATE TABLE MouldVision.references (
id_reference SERIAL PRIMARY KEY NOT NULL,
form_no INT NOT NULL REFERENCES MouldVision.forms(number),
reference CHAR(20) NOT NULL,
description CHAR(30) NOT NULL,
cycle_time NUMERIC(5,2),
cavity INT
);
CREATE TABLE MouldVision.register  (
id_register BIGSERIAL PRIMARY KEY NOT NULL,
id_user INT NOT NULL REFERENCES MouldVision.users(id_user),
id_machine INT NOT NULL REFERENCES MouldVision.machines(id_machine),
of_number INT NOT NULL,
of_quantity INT NOT NULL,
prod_date_start DATE NOT NULL,
prod_date_end DATE NOT NULL,
details_total INT NOT NULL,
details_waste_machine INT NOT NULL,
details_waste_human INT NOT NULL,
details_waste_material INT NOT NULL,
details_waste_process INT NOT NULL,
availability_waiting_material INT NOT NULL,
availability_settings INT NOT NULL,
availability_machine_failure INT NOT NULL,
availability_other INT NOT NULL,
availability_comments CHAR (300) NOT NULL
);