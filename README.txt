Mouldvision � OEE recording system
1.	PODSTAWOWE ZA�O�ENIA I FUNKCJONALNO��.
a.	Aplikacja obslugiwana przez urz�dzenie z dotykowym ekranem. 
	Prosty i intuicyjny interfejs graficzny - system wykorzystywany b�dzie przez pracownik�w produkcji (operator�w wtryskarek).
b.	System rejestruje czas przezbroje� (w celu analizy programu SMED)
c.	System rejestruje czasy dost�pno�� � poprzez rozpoczynanie, wstrzymywanie i ko�czenie produkcji. 
	Poszczeg�lne odst�py czasowe rejestrowane s� w bazie danych.
d.	System oblicza wydajno�� produkcji � na podstawie bazy danych (czas cyklu maszyny) oraz ilo�ci wyprodukowanych detali
e.	System oblicza wska�nik jako�ci produkcji � na podstawie ilo�ci odpad�w w stosunku do og�lnej ilo�ci wyprodukowanych detali.
f.	Aplikacja udost�pnia dane na potrzeby analizy OEE.
g.	Aplikacja mo�e by� rozbudowana o elementy analizy OEE ( budowanie wykres�w, tabel itd).

3.	MO�LIWY PROCES ROZWOJU APLIKACJI
a.	w najprostszym przypadku aplikacja okienkowa, desktopowa, oddzielna dla ka�dej maszyny, uruchamiana na tablecie 
	z systemem Windows, rejestruj�ca w�asn� baz� danych i udostepniaj�ca dane przez e-mail
b.	w kolejnym etapie polaczenie 4 tablet�w w sie� domow� i ��czenie 4 baz danych do jednej i udost�pnianie mailem
c.	w nast�pnym przechowywanie danych na serwerze
d.	w docelowym formacie aplikacja webowa z dost�pem dla kilku operator�w jednocze�nie, z w�asnym serwerem danych
	umo�liwiaj�cym edycje przez kilku u�ytkownik�w jednocze�nie. Aplikacja umo�liwia�aby drukowanie odpowiednich raport�w. 
	Dodatkowym narz�dziem dla operator�w maszyn mog�aby by� aplikacja mobilna, kt�ra wysy�a�aby powiadomienia 
	o post�pie produkcji, konieczno�ci jej zako�czenia i wymaganego przezbrojenia, uzupe�nienia surowca.

