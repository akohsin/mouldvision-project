package app.GUI;

import app.Logger;

public class UIFunction {
    Logger logger = new Logger();

    public void startProduction() {

    }

    public void pauseProduction(boolean loggOut) {
        if (loggOut) {
            logger.setLogIn(false);
        }
    }

    public void startChangeOver() {

    }

    public void stopChangeOver() {

    }

    public void confirmProduction() {

    }

    public void confirmDetails() {

    }

    public void confirmWastes() {

    }

    public void loggOperator() {
        logger.setLogIn(true);
    }

    public void chooseMachine() {

    }

    public void loggOutOperator() {

    }

    public boolean isLogIn() {
        return logger.isLogIn();
    }

}
