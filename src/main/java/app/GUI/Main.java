package app.GUI;

import app.Config;

public class Main {
    public static void main(String[] args) {
        Config.getINSTANCE().loadConfiguration();


        WindowFrame frame = new WindowFrame();
        frame.setVisible(true);

    }
}
