package app.GUI;

import app.Config;
import app.Machine;
import app.Operator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Window {

    private JPanel all;
    private JPanel mainPane;
    private JPanel cardPane;
    private JLabel headingLabel;
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField4;
    private JTextField textField3;
    private JTextField textField5;
    private JTextField textField6;
    private JTextField textField7;
    private JButton PROBLEMZDOSTAWCĄButton;
    private JButton OCZEKIWANIENAMATERIAŁButton;
    private JButton PROBLEMJAKOŚCIOWYButton;
    private JButton USTERKAMASZYNYButton;
    private JButton USTERKAFORMYButton;
    private JButton USTAWIENIAButton;
    private JPanel confirmingPane;
    private JRadioButton odpadyRadioButton;

    private CardLayout cardLayout;
    //panel
    private JPanel produtionPane;
    private JFormattedTextField formattedTextFieldOF;
    private JFormattedTextField formattedTextFieldIOfPcs;
    private JLabel machineHeadingLabel;
    private JLabel operatorLabel;
    private JLabel ofNumberLabel;
    private JLabel ofQtyLabel;
    private JLabel refLabel;
    private JRadioButton singleOperatorRadioButtom;
    private JRadioButton doubleOperatorRadioButtom;
    private JProgressBar progressBar1;
    private JButton changeOverButtom;
    private JButton confirmingButtom;
    private JTextArea statusBar;
    private JButton logInButtom;
    private JButton enableChangeOverButtom;
    private JButton startChangeOverButtom;
    private JCheckBox formChangeChangeBox;
    private JCheckBox colorChangeoverCheckBox;
    private JComboBox operatorComboBox;
    private JComboBox comboBoxReference;
    private JComboBox machinesComboBox;
    private JButton machineChooseButtom;


    public JPanel getMainPane() {
        return mainPane;
    }

    public Window() {

        //stan początkowy

        setStartPoint();

        operatorComboBox.addItem(null);
        machinesComboBox.addItem(null);

        boolean isLogIn = false;
        UIFunction uiFunction = new UIFunction();


        for (Operator operator : Config.getINSTANCE().getOperatorsList()) {
            operatorComboBox.addItem(operator.toString());
        }
        for (Machine machine : Config.getINSTANCE().getMachineList()) {
            machinesComboBox.addItem(machine.toString());
        }

        logInButtom.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!uiFunction.isLogIn()) {
                    if (operatorComboBox.getSelectedItem() == null) {
                        statusBar.setText("wybierz użytkownika");
                    } else {
                        logInButtom.setText("Wyloguj");
                        operatorComboBox.setEnabled(false);
                        machinesComboBox.setEnabled(true);
                        machineChooseButtom.setEnabled(true);
                        uiFunction.loggOperator();
                    }
                } else {
                    uiFunction.pauseProduction(true);
                    logInButtom.setText("Zaloguj");
                    setStartPoint();

                }

            }
        });


    }

    private void setStartPoint() {
        headingLabel.setText("MouldVision");
        refLabel.setText("Wybierz Referencje");
        ofQtyLabel.setText("Wprowadź ilość detali");
        operatorLabel.setText("Wybierz Operatora");
        changeOverButtom.setEnabled(false);
        formattedTextFieldOF.setEnabled(false);
        formattedTextFieldIOfPcs.setEnabled(false);
        doubleOperatorRadioButtom.setEnabled(false);
        singleOperatorRadioButtom.setEnabled(false);
        comboBoxReference.setEnabled(false);
        machinesComboBox.setEnabled(false);
        enableChangeOverButtom.setEnabled(false);
        startChangeOverButtom.setEnabled(false);
        formChangeChangeBox.setEnabled(false);
        colorChangeoverCheckBox.setEnabled(false);
        confirmingButtom.setEnabled(false);
        statusBar.setEnabled(false);
        machineChooseButtom.setEnabled(false);
        operatorComboBox.setEnabled(true);
        operatorComboBox.setEnabled(true);
    }
}
//            operators.addItem(operator.toString());

//            machines.addItem(machine.getName());
//        cardLayout = (CardLayout) cardPane.getLayout();
//        String name = operators.getSelectedItem().toString().split(" ")[0];
//        String surname = operators.getSelectedItem().toString().split(" ")[1];
//        String machine = machines.getSelectedItem().toString();
//        Logger.next(new Machine(machine), new Operator(name, surname));
//        cardLayout.show(loggPane.getParent(), "Card2");


