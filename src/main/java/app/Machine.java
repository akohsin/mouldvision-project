package app;

public class Machine {
    private String name;

    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    public Machine(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
