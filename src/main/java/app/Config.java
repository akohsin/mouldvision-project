package app;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Config {
    private static final Config INSTANCE = new Config();
    private static String message;
    private List<Operator> operatorsList = new ArrayList();
    private List<Machine> machineList = new ArrayList();

    private Config() {
    }

    public static Config getINSTANCE() {
        return INSTANCE;
    }

    public void loadConfiguration(){
        loadMachines();
        loadOperators();
    }

    private void loadMachines() {
        try (BufferedReader reader = new BufferedReader(new FileReader(new File("machines")))) {
            while (true) {
                String line = reader.readLine();
                if (line == null) {
                    break;
                }
                machineList.add(new Machine(line));
                System.out.println(line);

            }
        } catch (FileNotFoundException fnfe) {
            message = "Plik z listą maszyn nie odnaleziony";
        } catch (IOException ioe) {
            message = "Wystąpił błąd strumienia wejścia/wyjścia";
        }
    }

    private void loadOperators() {
        try (BufferedReader reader = new BufferedReader(new FileReader(new File("operators")))) {
            while (true) {
                String line = reader.readLine();
                if (line == null) {
                    break;
                }
                System.out.println(line);
                String[] operator = line.split(" ");
                operatorsList.add(new Operator(operator[0], operator[1]));

            }
        } catch (FileNotFoundException fnfe) {
            message = "Plik z listą operatorów nie odnaleziony";
        } catch (IOException ioe) {
            message = "Wystąpił błąd strumienia wejścia/wyjścia";
        }
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public List<Operator> getOperatorsList() {
        return operatorsList;
    }

    public List<Machine> getMachineList() {
        return machineList;
    }

    private void setOperatorsList(List<Operator> operatorsList) {
        this.operatorsList = operatorsList;
    }

    private void setMachineList(List<Machine> machineList) {
        this.machineList = machineList;
    }
}
