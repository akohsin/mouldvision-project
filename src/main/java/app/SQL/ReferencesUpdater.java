package app.SQL;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

public class ReferencesUpdater {
    public void updateReferences() {
        Reader in = null;

        List<String[]> referenceTable = new LinkedList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("references.csv"))) {
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] words = line.split(";");
                referenceTable.add(words);
                line = bufferedReader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ResultSet rsSelect = null;
        try (Connection c = DbConnectionProvider.getConnection();
             PreparedStatement pst1 = c.prepareStatement("SELECT * FROM mouldvision.references");
             PreparedStatement pst2 = c.prepareStatement("UPDATE mouldvision.references " +
                     "SET cycle_time = ?, cavity = ?, description = ? WHERE reference = ?")) {
            rsSelect = pst1.executeQuery();
            while (rsSelect.next()) {
                String tmp = rsSelect.getString("reference").trim();
                for (String[] record : referenceTable) {
                    if (record[0].equals(tmp))
                        if (rsSelect.getString("description").equals(record[1]) ||
                                rsSelect.getString("cavity").equals(record[3]) ||
                                rsSelect.getString("cycle_time").equals(record[4])) {

                        } else {
                            pst2.setString(4, record[0]);
                            pst2.setString(3, record[1]);
//                pst.setInt(3, Integer.parseInt(values[2]));
                            pst2.setInt(2, Integer.parseInt(record[3]));
                            pst2.setInt(1, Integer.parseInt(record[4]));
                            pst2.addBatch();
                        }
                }
            }
            pst2.executeBatch();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
