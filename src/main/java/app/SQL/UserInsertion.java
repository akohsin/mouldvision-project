package app.SQL;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class UserInsertion {

    //private String stm = "INSERT INTO MouldVision.users(username,surname) VALUES(?,?)";

    public void insertUser(String name, String surname) {

        try (Connection c = DbConnectionProvider.getConnection();
             PreparedStatement pst = c.prepareStatement
                     ("INSERT INTO MouldVision.users(username,surname) VALUES(?,?)")) {
            pst.setString(1, name);
            pst.setString(2, surname);
            pst.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            //do dodanie wątek obsługujący status bar
//        }
        }
    }
}
