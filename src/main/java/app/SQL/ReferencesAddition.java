package app.SQL;


import java.io.*;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class ReferencesAddition {
    public void addReferences() {
        Reader in = null;
        List<String[]> referenceTable = new LinkedList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("references.csv"))) {
            String line = bufferedReader.readLine();
            while (line != null) {
                String[] words = line.split(";");
                referenceTable.add(words);
                line = bufferedReader.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (Connection c = DbConnectionProvider.getConnection();
             PreparedStatement pst = c.prepareStatement("INSERT INTO mouldvision.references(reference, description, form_no, cavity, cycle_time) VALUES(?,?,?,?,?)")) {

            for (String[] values : referenceTable) {
                pst.setString(1, values[0]);
                pst.setString(2, values[1]);
                pst.setInt(3, Integer.parseInt(values[2]));
                pst.setInt(4, Integer.parseInt(values[3]));
                pst.setInt(5, Integer.parseInt(values[4]));
                pst.addBatch();
            }
            pst.executeBatch();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}