package app.SQL;

import org.apache.ibatis.jdbc.ScriptRunner;


import java.io.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import static java.sql.DriverManager.getConnection;

public class DbCreation {
    public static ScriptRunner prepareDatabase() throws SQLException, IOException {
        return preparePostgresSqlDatabase();
    }

    public static ScriptRunner preparePostgresSqlDatabase() throws SQLException, IOException {
        Connection connection = getConnection("postgres_init.properties");
        ScriptRunner scriptRunner = new ScriptRunner(connection);
        scriptRunner.setLogWriter(new PrintWriter(new FileWriter(new File("log.txt"))));
        scriptRunner.runScript(new InputStreamReader(UserInsertion.class.getResourceAsStream("/DB_creation.sql")));
        return scriptRunner;
    }

    private static Connection getConnection(String fileName) throws IOException, SQLException {
        InputStream inputStream = DbConnectionProvider.class.getClassLoader().getResourceAsStream(fileName);
        Properties properties = new Properties();
        properties.load(inputStream);
        String url = properties.getProperty("url");
        String user = properties.getProperty("user");
        String password = properties.getProperty("password");
        return DriverManager.getConnection(url, user, password);
    }
}
