package app;

public class Operator {
    String imie;
    String nazwisko;

    public String getImie() {
        return imie;
    }

    private void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    private void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }


    @Override
    public String toString() {
        return imie+" "+nazwisko;
    }

    public Operator(String imie, String nazwisko) {
        this.imie = imie;
        this.nazwisko = nazwisko;
    }

}
